from flask import Blueprint, jsonify
from pymongo import MongoClient
from bson.json_util import dumps, CANONICAL_JSON_OPTIONS
import json, re

view_groceries_bp = Blueprint("view_groceries", __name__)

@view_groceries_bp.route("/viewGroceries",)
def view():
    print("view grocery method called")

    client = MongoClient()
    db = client.test_database
    collection = db.user_profile
    print(collection)
    data = collection.aggregate([{'$lookup': { 'from' : 'grocery_list', 'localField': "_id", 'foreignField':'user_id', 'as': "Purchased Data"}}])
    clean_json = re.compile('ISODate\(("[^"]+")\)').sub('\\1', dumps(data, json_options=CANONICAL_JSON_OPTIONS))
    json_obj = json.loads(clean_json)
    return jsonify(json_obj)