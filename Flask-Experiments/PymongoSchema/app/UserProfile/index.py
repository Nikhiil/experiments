from flask import Blueprint


index_bp = Blueprint("index", __name__)


# landing page
@index_bp.route('/')
@index_bp.route('/home')
def home():
    return "Home Page"

# about page
@index_bp.route('/about')
def about():
    return "About Page"
