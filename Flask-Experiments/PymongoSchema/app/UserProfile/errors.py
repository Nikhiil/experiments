from flask import Blueprint

errorHandler_bp = Blueprint('errorHandler', __name__)

# to handle non-existent pages
@errorHandler_bp.errorhandler(404)
def not_found(e):
    return "<h1> The page you're looking for doesn't exist. <h1>"

@errorHandler_bp.errorhandler(405)
def mehtod_not_allowed(e):
    return " <h1> Current request is not allowed for this method </h1>"