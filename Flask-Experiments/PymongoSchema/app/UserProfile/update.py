from bson import ObjectId
from flask import Blueprint, jsonify
from flask import request
from pymongo import MongoClient, ReturnDocument
from bson.json_util import dumps, CANONICAL_JSON_OPTIONS
import re, json


update_bp = Blueprint('update',__name__)


@update_bp.route('/update', methods=['GET'])
def update():
    print("Entering update function")
    client = MongoClient()
    db = client.test_database
    collection = db.user_profile
    data = collection.find_one_and_update({'_id': ObjectId(request.args.get(id))},{'$set': {'first_name' : request.args.get('first_name')}})
    print(data)
    clean_json = re.compile('ISODate\(("[^"]+")\)').sub('\\1', dumps(data, json_options=CANONICAL_JSON_OPTIONS))
    json_obj = json.loads(clean_json)
    return jsonify(json_obj)

# error handlers within a single flask Blueprint
@update_bp.app_errorhandler(404)
def not_found(e):
    return "<h1> The page you're looking for doesn't exist. <h1>"

@update_bp.app_errorhandler(405)
def mehtod_not_allowed(e):
    return " <h1> Current request is not allowed for this method </h1>"