from flask import Blueprint
from flask import request
from pymongo import MongoClient
import re
import json


upload_bp = Blueprint("upload_bp", __name__)

# upload data to database
@upload_bp.route("/upload", methods=['POST'])
def upload():
    print("Calling upload")
    client = MongoClient()
    db = client.test_database
    collection = db.test_collection

    print(collection)
    print(db)

    response = {
        'result' : 'fail',
        'reason': ''
    }
    # first_name
    if not request.form.get('first_name', None):
        response['reason'] = 'First name is required'
        return response
    else:
        first_name = request.form.get('first_name')
        if len(first_name) > 15:
            response['reason'] = 'First name should be less than 15 characters'
            return response


    #last_name
    if not request.form.get('last_name', None):
        response['reason'] = 'Last name field is required'
        return response
    else:
        last_name = request.form.get('last_name')
        if len(last_name) > 15:
            response['reason'] = 'Last name should be less than 15 characters'
            return response


    # email
    if not request.form.get('email', None):
        response['reason'] = 'email field is required'
        return response
    else:
        email = request.form.get('email')
        regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
        if (re.search(regex, email)) is None:
            response['reason'] = 'Invalid Email'
            return response

    # phone_number
    if not request.form.get('phone_number', None):
        response['reason'] = 'Phone number is required'
        return response
    else:
        phone_number = json.loads(request.form.get('phone_number'))
        # to check if the phone number has only numbers
        if not phone_number['phone_number'].isdigit():
            response['reason'] = 'Phone number can only consist of numbers'
            return response
        else:
            # verify whether it is a 10-digit phone starting with either 7 or 8 or 9
            phone_pattern = '^[7,8,9](\d{9})$'
            if not re.fullmatch(phone_pattern, phone_number['phone_number']):
                response['reason'] = 'Invalid phone number'
                phone_number = ''
                return response

    # country
    if not request.form.get('country', None):
        response['reason'] = 'country field is required'
        return response
    else:
        country = request.form.get('country')

    # email_verified
    if not request.form.get('email_verified', None) :
        response['reason'] = 'email verified field is required'
        return response
    else:
        email_verified = request.form.get('email_verified')
    # citizenship_id
    if not request.form.get('citizenship_id', None):
        response['reason'] = 'Citizenship ID is required'
        return response
    else:
        citizenship_id = request.form.get('citizenship_id')

    # user_id
    if not request.form.get('user_id', None):
        response['reason'] = 'User ID is required'
        return response
    else:
        user_id = request.form.get('user_id')

    # gender
    if not request.form.get('gender', None):
        response['reason'] = 'Gender is required'
        return response
    else:
        gender = request.form.get('gender')

    # blood_group
    if not request.form.get('blood_group', None):
        response['reason'] = 'Blood Group is required'
        return response
    else:
        blood_group = request.form.get('blood_group')

    # date_of_birth
    if not request.form.get('date_of_birth', None):
        response['reason'] = 'Date of birth is required'
        return response
    else:
        date_of_birth = request.form.get('date_of_birth')

    email_verified = request.form.get('email_verified') or ''
    user_id = request.form.get('user_id') or '' #optional
    citizenship_id = request.form.get('citizenship_id') or ''
    profile_pic = request.form.get('profile_pic') or '' # optional
    user_ids = request.form.get('user_ids')  or '' # optional
    martial_status = request.form.get('martial_status') or '' #optional
    family_information = request.form.get('family_information')  or '' #optional
    address = request.form.get('address') or '' #optional
    kin_information = request.form.get('kin_information') or '' #optional
    company_name = request.form.get('company_name')  or '' #optional
    profession = request.form.get('profession') or '' #optional
    company_address = request.form.get('company_address') or '' #optional
    contact_information_address = request.form.get('contact_information_address') or '' #optional

    data = {
        "first_name" : first_name,
        "last_name" : last_name,
        "email" : email,
        "email_verified" : email_verified,
        "blood_group" : blood_group,
        "phone_number" : phone_number,
        "date_of_birth" : date_of_birth,
        "country" : country,
        "user_id" : user_id,
        "citizenship_id" : citizenship_id,
        "martial_status" : martial_status,
        "user_ids" : user_ids,
        "family_information" : family_information,
        "address" : address,
        "kin_information" : kin_information,
        "profession" : profession,
        "company_address" : company_address,
        "contact_information_address" : contact_information_address,
        "company_name" : company_name,
        "profile_pic" : profile_pic
    }
    if collection.insert_one(data):
        response['result'] = 'pass'
    return response

# error handlers within a single flask Blueprint
@upload_bp.app_errorhandler(404)
def not_found(e):
    return "<h1> The page you're looking for doesn't exist. <h1>"

@upload_bp.app_errorhandler(405)
def mehtod_not_allowed(e):
    return " <h1> Current request is not allowed for this method </h1>"