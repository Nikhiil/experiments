from flask import Flask

from app.UserProfile.index import index_bp
from app.UserProfile.errors import errorHandler_bp
from app.UserProfile.uploads import upload_bp
from app.UserProfile.update import update_bp
from app.UserProfile.viewGroceries import view_groceries_bp

flask_app =  Flask(__name__)

flask_app.register_blueprint(index_bp)
flask_app.register_blueprint(upload_bp)
flask_app.register_blueprint(errorHandler_bp)
flask_app.register_blueprint(update_bp)
flask_app.register_blueprint(view_groceries_bp)